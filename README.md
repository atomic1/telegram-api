# Telegram API

We are often looking for fast, free and easy way to get notifications from our applications or systems we made to our mobile phone. Using WhatsApp API is not free, but also it is not fast enough. After some exploration of different possibilities I came with the best notification solution that currently exists. It is via Telegram.

How to use Telegram to send quick notification messages for free from your lines of code. Get notification in a matter of milliseconds on your mobile device. 


## Getting started

There are two thins you need to prepare to be able to use this API

- Bot (with bot token)
- Group (and get group ID)

## First you need to create new group on Telegram
 
 - Add new group. Give name you want. 

 - Get Group ID, using the BotFather ( subname bot ). It is the main Telegram bot that will automatic
  Search for the user BotFather, private message, and create the new bot :
     type:

     /start
     (you will get list of the commands and then you need to create new bot) 

     /newbot
     (then give the name, name must have BOT in the end)
     you will receive now message :
     
     Done! Congratulations on your new bot ....

     and in the end you will receive you <b>HTTP API token</b>. Save this token somewhere safe!

      to get list of all commands by the BotFather type:
      /help

  - Add new created bot to group you created in the first step. You can either add bot as an Admin of the group, or change the permissions what it is supposed to be able to do.

   - To get group chat_id, the command is ->
   - ![#f03c15]

     curl 'https://api.telegram.org/bot'$BOT_TOKEN'/getUpdates'

     it should look like this when variables are replaced -> 
     curl 'https://api.telegram.org/bot55######:AAX######################U3Ds0a0/getUpdates'
     
     in the Json you will receive look for the <b>chat object</b> and get the group_ID from it.
     bot to the group again.
      
     reply from this query should look like this ->
     {"update_id":8393,"message":{"message_id":3,"from":{"id":7474,"first_name":"AAA"},"chat":{"id":<group_ID>,"title":""},"date":25497,"new_chat_participant":{"id":71,"first_name":"NAME","username":"YOUR_BOT_NAME"}}}


     
      - And that's it. Bellow is examle written in Bash, but when you know how curl fucntion is working, it is super easy to replicate this in any language, especially in PHP, Python or Java

         

## BASH example


#!/bin/bash

<text># Token and Group ID settings, please put your values in here -><br>
BOT_TOKEN=5557510:AAFStBRg22VUULd8nS0a0
<br>
GROUP_ID=-1001784

<br>
if [ "$1" == "-h" ]; then
<br>
  echo "Help. Use it as: `basename $0` \"Your text message under the quotes\""
<br>
  exit 0
<br>
fi
<br><br>
if [ -z "$1" ]
  then
<br>
    echo "Please add message as an argument to this command"
<br>
    exit 0
<br>
fi
<br>
<br>
if [ "$#" -ne 1 ]; then
<br>
    echo "Please use the quotes around your text message. You can send only one at the time!"
<br>
    exit 0
<br>
fi
<br>
<br>
# Send Message ->
<br>
curl --data "text=$1" --data "chat_id=$GROUP_ID" 'https://api.telegram.org/bot'$BOT_TOKEN'/sendMessage'
<br>
<br>
# Send Image ->
<br>
curl -s --form photo=@"yourimage.jpg" --form "caption=title_imange" --form-string "chat_id=$GROUP_ID" 'https://api.telegram.org/bot'$BOT_TOKEN'/sendPhoto'
<br>
</text>


##  Possibility to receive message 

  - To receive message from the group is even simplier. You just need to pickup the json from:

   'https://api.telegram.org/bot'$BOT_TOKEN'/getUpdates'

    The response will look like ->

     {"ok":true,"result":[{"update_id":661###307, "message":{"message_id":1283,"from":{"id":16#####63,"is_bot":false, "first_name":"An\u0111ela","last_name":"Tomi\u0107","username":"###iak###"},"chat":{"id":-100#######,"title":"GroupName" "type":"supergroup"},"date":1657317772,"text":"/dothis","entities":[{"offset":0,"length":6,"type":"bot_command"}]}}]}


